// Import thư viện Express Js
const express = require("express");

// Khởi tạo 1 app express
const app = express();

//Khai báo 1 cổng 
const port = 8000;

//Khai báo router app
const drinkRouter = require ("./app/routes/drinkRouter");
const voucherRouter = require ("./app/routes/voucherRouter");
const userRouter = require ("./app/routes/userRouter");
const orderRouter = require ("./app/routes/orderRouter");
// Khai báo GET API trả ra thông tin chạy được trên cổng 
app.get("/", (request, response) => {

    response.json({
        message: "App đã chạy!!!"
    })
})

// App sử dụng router
app.use("/api", drinkRouter);
app.use("/api", voucherRouter);
app.use("/api",userRouter);
app.use("/api", orderRouter);

// Chạy app trên cổng đã khai báo
app.listen(port, () =>{
    console.log(`App đã chạy trên cổng ${port}`);
})