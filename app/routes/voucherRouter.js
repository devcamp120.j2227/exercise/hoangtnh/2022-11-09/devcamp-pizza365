// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

router.get("/vouchers", (request, response) => {
    response.json({
        message: "Get ALL Vouchers"
    })
})

router.post("/vouchers", (request, response) => {
    response.json({
        message: "Create New Voucher"
    })
})

router.get("/vouchers/:voucherId", (request, response) => {
    const voucherId = request.params.voucherId;

    response.json({
        message: "Get Voucher ID = " + voucherId
    })
})

router.put("/vouchers/:voucherId", (request, response) => {
    const voucherId = request.params.voucherId;

    response.json({
        message: "Update Voucher ID = " + voucherId
    })
})

router.delete("/vouchers/:voucherId", (request, response) => {
    const voucherId = request.params.voucherId;

    response.json({
        message: "Delete Voucher ID = " + voucherId
    })
})

module.exports = router;