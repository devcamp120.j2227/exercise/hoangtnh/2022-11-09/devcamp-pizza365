// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

router.get("/orders", (request, response) => {
    response.json({
        message: "Get ALL Orders"
    })
})

router.post("/orders", (request, response) => {
    response.json({
        message: "Create New Order"
    })
})

router.get("/orders/:orderId", (request, response) => {
    const orderId = request.params.orderId;

    response.json({
        message: "Get Order ID = " + orderId
    })
})

router.put("/orders/:orderId", (request, response) => {
    const orderId = request.params.orderId;

    response.json({
        message: "Update Order ID = " + orderId
    })
})

router.delete("/orders/:orderId", (request, response) => {
    const orderId = request.params.orderId;

    response.json({
        message: "Delete Order ID = " + orderId
    })
})

module.exports = router;