// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

router.get("/users", (request, response) => {
    response.json({
        message: "Get ALL Users"
    })
})

router.post("/users", (request, response) => {
    response.json({
        message: "Create New User"
    })
})

router.get("/users/:userId", (request, response) => {
    const userId = request.params.userId;

    response.json({
        message: "Get User ID = " + userId
    })
})

router.put("/users/:userId", (request, response) => {
    const userId = request.params.userId;

    response.json({
        message: "Update User ID = " + userId
    })
})

router.delete("/users/:userId", (request, response) => {
    const userId = request.params.userId;

    response.json({
        message: "Delete User ID = " + userId
    })
})

module.exports = router;