// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

router.get("/drinks", (request, response) => {
    response.json({
        message: "Get ALL Drinks"
    })
})

router.post("/drinks", (request, response) => {
    response.json({
        message: "Create New Drink"
    })
})

router.get("/drinks/:drinkId", (request, response) => {
    const drinkId = request.params.drinkId;

    response.json({
        message: "Get Drink ID = " + drinkId
    })
})

router.put("/drinks/:drinkId", (request, response) => {
    const drinkId = request.params.drinkId;

    response.json({
        message: "Update Drink ID = " + drinkId
    })
})

router.delete("/drinks/:drinkId", (request, response) => {
    const drinkId = request.params.drinkId;

    response.json({
        message: "Delete Drink ID = " + drinkId
    })
})

module.exports = router;
